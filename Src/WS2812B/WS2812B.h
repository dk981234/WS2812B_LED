/*
 * WS2812B.h
 *
 *  Created on: 28 ����. 2019 �.
 *      Author: sloth
 */

#ifndef WS2812B_WS2812B_H_
#define WS2812B_WS2812B_H_
#define LEDS 16
#define PREAMBLESIZE	500

extern uint8_t buf[ PREAMBLESIZE +24*LEDS];

void setLEDColor(uint16_t numLED, uint8_t red, uint8_t green, uint8_t blue);
void setOnlyGreenColor(uint16_t numLED, uint8_t green);
void setOnlyBlueColor(uint16_t numLED, uint8_t blue);
void setOnlyRedColor(uint16_t numLED, uint8_t red);
void rainbow();
void clear();
void red_headed_snake();
void roll();
void brightness(int numLED, double  v);
#endif /* WS2812B_WS2812B_H_ */
