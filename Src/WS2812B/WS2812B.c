#include "stm32f1xx_hal.h"
#include "stm32f1xx_hal_rcc.h"
/*

* WS2812B.c
 *
 *  Created on: 28 ����. 2019 �.
 *      Author: sloth
 */

#define PREAMBLESIZE	500
#define LEDS	16

#define WS2812B_OFF		4
#define WS2812B_ON		8

uint8_t buf[PREAMBLESIZE + LEDS * 24];

void setLEDColor(uint16_t numLED, uint8_t red, uint8_t green, uint8_t blue) {
	if (numLED < LEDS) {
		uint16_t i = PREAMBLESIZE + numLED * 24;
		// GREEN
		for(int j=0;j<8;j++){
			buf[i+7-j] = (((green >> j) & 1) > 0) ? WS2812B_ON : WS2812B_OFF; //WS2812B_ON;//10;
		}
		i=PREAMBLESIZE + numLED * 24+8;
		//RED
		for(int j=0;j<8;j++){
			buf[i+7-j] = (((red >> j) & 1) > 0) ? WS2812B_ON : WS2812B_OFF; //WS2812B_ON;//10;
		}
		i=PREAMBLESIZE + numLED * 24+16;
		//BLUE
		for(int j=0;j<8;j++){
			buf[i+7-j] = (((blue >> j) & 1) > 0) ? WS2812B_ON : WS2812B_OFF; //WS2812B_ON;//10;
		}
//		buf[i + 7] = (((green >> 0) & 1) > 0) ? WS2812B_ON : WS2812B_OFF; //WS2812B_OFF;//10;
//		buf[i + 6] = (((green >> 1) & 1) > 0) ? WS2812B_ON : WS2812B_OFF; //WS2812B_OFF;//10;
//		buf[i + 5] = (((green >> 2) & 1) > 0) ? WS2812B_ON : WS2812B_OFF; //WS2812B_OFF;//10;
//		buf[i + 4] = (((green >> 3) & 1) > 0) ? WS2812B_ON : WS2812B_OFF; //WS2812B_OFF;//10;
//		buf[i + 3] = (((green >> 4) & 1) > 0) ? WS2812B_ON : WS2812B_OFF; //WS2812B_OFF;//10;
//		buf[i + 2] = (((green >> 5) & 1) > 0) ? WS2812B_ON : WS2812B_OFF; //WS2812B_OFF;//10;
//		buf[i + 1] = (((green >> 6) & 1) > 0) ? WS2812B_ON : WS2812B_OFF; //WS2812B_OFF;//10;
//		buf[i] = (((green >> 7) & 1) > 0) ? WS2812B_ON : WS2812B_OFF; //WS2812B_ON;//10;

		// RED
//		buf[i + 15] = (((red >> 0) & 1) > 0) ? WS2812B_ON : WS2812B_OFF; //WS2812B_OFF;//10;
//		buf[i + 14] = (((red >> 1) & 1) > 0) ? WS2812B_ON : WS2812B_OFF; //WS2812B_OFF;//10;
//		buf[i + 13] = (((red >> 2) & 1) > 0) ? WS2812B_ON : WS2812B_OFF; //WS2812B_OFF;//10;
//		buf[i + 12] = (((red >> 3) & 1) > 0) ? WS2812B_ON : WS2812B_OFF; //WS2812B_OFF;//10;
//		buf[i + 11] = (((red >> 4) & 1) > 0) ? WS2812B_ON : WS2812B_OFF; //WS2812B_OFF;//10;
//		buf[i + 10] = (((red >> 5) & 1) > 0) ? WS2812B_ON : WS2812B_OFF; //WS2812B_OFF;//10;
//		buf[i + 9] = (((red >> 6) & 1) > 0) ? WS2812B_ON : WS2812B_OFF; //WS2812B_OFF;//10;
//		buf[i + 8] = (((red >> 7) & 1) > 0) ? WS2812B_ON : WS2812B_OFF; //WS2812B_ON;//10;

		// BLUE
//		buf[i + 23] = (((blue >> 0) & 1) > 0) ? WS2812B_ON : WS2812B_OFF; //WS2812B_OFF;//10;
//		buf[i + 22] = (((blue >> 1) & 1) > 0) ? WS2812B_ON : WS2812B_OFF; //WS2812B_OFF;//10;
//		buf[i + 21] = (((blue >> 2) & 1) > 0) ? WS2812B_ON : WS2812B_OFF; //WS2812B_ON;//10;
//		buf[i + 20] = (((blue >> 3) & 1) > 0) ? WS2812B_ON : WS2812B_OFF; //WS2812B_OFF;//10;
//		buf[i + 19] = (((blue >> 4) & 1) > 0) ? WS2812B_ON : WS2812B_OFF; //WS2812B_OFF;//10;
//		buf[i + 18] = (((blue >> 5) & 1) > 0) ? WS2812B_ON : WS2812B_OFF; //WS2812B_OFF;//10;
//		buf[i + 17] = (((blue >> 6) & 1) > 0) ? WS2812B_ON : WS2812B_OFF; //WS2812B_OFF;//10;
//		buf[i + 16] = (((blue >> 7) & 1) > 0) ? WS2812B_ON : WS2812B_OFF; //WS2812B_ON;//10;
	}
}

void setOnlyRedColor(uint16_t numLED, uint8_t red) {
	if (numLED < LEDS) {
		i=PREAMBLESIZE + numLED * 24+8;
		for(int j=0;j<8;j++){
			buf[i+7-j] = (((red >> j) & 1) > 0) ? WS2812B_ON : WS2812B_OFF; //WS2812B_ON;//10;
		}

	}
}
void setOnlyGreenColor(uint16_t numLED, uint8_t green) {
	if (numLED < LEDS) {
		uint16_t i = PREAMBLESIZE + numLED * 24;
		for(int j=0;j<8;j++){
			buf[i+7-j] = (((green >> j) & 1) > 0) ? WS2812B_ON : WS2812B_OFF; //WS2812B_ON;//10;
		}
		i=PREAMBLESIZE + numLED * 24+8;
	}
}
void setOnlyBlueColor(uint16_t numLED, uint8_t blue) {
	if (numLED < LEDS) {
		uint16_t i=PREAMBLESIZE + numLED * 24+16;
		for(int j=0;j<8;j++){
			buf[i+7-j] = (((blue >> j) & 1) > 0) ? WS2812B_ON : WS2812B_OFF; //WS2812B_ON;//10;
		}
	}
}
void rainbow() {
	uint16_t count = 0;
	while (count < LEDS) {
		setLEDColor(count, 255, 0, 0);
		count++;
		setLEDColor(count, 255, 125, 0);
		count++;
		setLEDColor(count, 255, 255, 0);
		count++;
		setLEDColor(count, 0, 255, 0);
		count++;
		setLEDColor(count, 0, 255, 255);
		count++;
		setLEDColor(count, 0, 0, 255);
		count++;
		setLEDColor(count, 255, 0, 255);
		count++;
	}
}
void clear() {
	for (int i = 0; i < LEDS; i++) {
		setLEDColor(i, 0, 0, 0);
	}
}
void red_headed_snake() {
	clear();
	int i;
	for (i = 0; i < 20; i++) {
		int k = 1000 * i / 20;
		setOnlyBlueColor(i, k * 255 / 1000);
	}
	setOnlyRedColor(i, 255);
}
void roll() {
	uint8_t prev = buf[PREAMBLESIZE + 24 * LEDS - 1];
	for (int i = 0; i < 24 * LEDS; i++) {
		uint8_t temp = buf[PREAMBLESIZE + i];
		buf[PREAMBLESIZE + i] = prev;
		prev = temp;
	}
}
uint8_t getLedRedColor(int numLED) {
	uint8_t color = 0;
	uint16_t i = PREAMBLESIZE + numLED * 24;
	color += (buf[i + 8] == WS2812B_ON) ? 0x80 : 0;
	color += (buf[i + 9] == WS2812B_ON) ? 0x40 : 0;
	color += (buf[i + 10] == WS2812B_ON) ? 0x20 : 0;
	color += (buf[i + 11] == WS2812B_ON) ? 0x10 : 0;
	color += (buf[i + 12] == WS2812B_ON) ? 0x08 : 0;
	color += (buf[i + 13] == WS2812B_ON) ? 0x04 : 0;
	color += (buf[i + 14] == WS2812B_ON) ? 0x02 : 0;
	color += (buf[i + 15] == WS2812B_ON) ? 0x01 : 0;
	return color;
}
uint8_t getLedBlueColor(int numLED) {
	uint8_t color = 0;
	uint16_t i = PREAMBLESIZE + numLED * 24;
	color += (buf[i + 16] == WS2812B_ON) ? 0x80 : 0;
	color += (buf[i + 17] == WS2812B_ON) ? 0x40 : 0;
	color += (buf[i + 18] == WS2812B_ON) ? 0x20 : 0;
	color += (buf[i + 19] == WS2812B_ON) ? 0x10 : 0;
	color += (buf[i + 20] == WS2812B_ON) ? 0x08 : 0;
	color += (buf[i + 21] == WS2812B_ON) ? 0x04 : 0;
	color += (buf[i + 22] == WS2812B_ON) ? 0x02 : 0;
	color += (buf[i + 23] == WS2812B_ON) ? 0x01 : 0;
	return color;
}
uint8_t getLedGreenColor(int numLED) {
	uint8_t color = 0;
	uint16_t i = PREAMBLESIZE + numLED * 24;
	color += (buf[i] == WS2812B_ON) ? 0x80 : 0;
	color += (buf[i + 1] == WS2812B_ON) ? 0x40 : 0;
	color += (buf[i + 2] == WS2812B_ON) ? 0x20 : 0;
	color += (buf[i + 3] == WS2812B_ON) ? 0x10 : 0;
	color += (buf[i + 4] == WS2812B_ON) ? 0x08 : 0;
	color += (buf[i + 5] == WS2812B_ON) ? 0x04 : 0;
	color += (buf[i + 6] == WS2812B_ON) ? 0x02 : 0;
	color += (buf[i + 7] == WS2812B_ON) ? 0x01 : 0;
	return color;
}
void brightness(int numLED, double  v) {
	if (v <= 100.0 && v >= 0.0) {
				double g = getLedGreenColor(numLED);
				double r = getLedRedColor(numLED);
				double b = getLedBlueColor(numLED);
				uint8_t hi;
				double vmin, vinc, vdec, a;
				double h;
				double s;
				double  min, max, delta;

				min = r < g ? r : g;
				min = min < b ? min : b;

				max = r > g ? r : g;
				max = max > b ? max : b;
				delta = max - min;

				if (delta != 0.0) {
					if (max == r) {
					    if(g>=b){
						h = 60*((g - b) / delta);

					    }
					    else{
					      	h = 60*((g - b) / delta+6.0);
					    }
					} else if (max == g) {
						h = 60*((b - r) / delta + 2.0);
					} else {
						h = 60*((r - g) / delta + 4.0);
					}
				} else {
					h = 0;
				}
				if (max == 0) {
					s = 0;
				} else {
					s = (1 - (double)min / (double)max)*100;
				}

				hi = ((uint8_t) (h / 60)) % 6;
				vmin = ((100 - s) * v / 100);
				a = (v - vmin) * (((uint16_t) h) % 60) / 60;
				vinc = vmin + a;
				vdec = v-a;
				switch (hi) {
					case 0:
					r = (v*255/100);
					g =(vinc*255/100);
					b = (vmin*255/100);
					break;
				case 1:
					r = (vdec*255/100);
					g =(v*255/100);
					b = (vmin*255/100);
					break;
				case 2:
					r = (vmin*255/100);
					g = (v*255/100);
					b =(vinc*255/100);
					break;
				case 3:
					r = (vmin*255/100);
					g = (vdec*255/100);
					b =(v*255/100);
					break;
				case 4:
					r = (vinc*255/100);
					g = (vmin*255/100);
					b = (v*255/100);
					break;
				case 5:
					r =  (v*255/100);
					g = (vmin*255/100);
					b = (vdec*255/100);
					break;

				}
		setLEDColor(numLED,  (uint8_t)r,  (uint8_t)g, (uint8_t) b);
	}
}
void all_brightness(double v){
	for(int i=0;i<LEDS;i++){
		brightness(LEDS,v);
	}
}

