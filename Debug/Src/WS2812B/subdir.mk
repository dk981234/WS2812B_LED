################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Src/WS2812B/WS2812B.c 

OBJS += \
./Src/WS2812B/WS2812B.o 

C_DEPS += \
./Src/WS2812B/WS2812B.d 


# Each subdirectory must supply rules for building sources it contributes
Src/WS2812B/%.o: ../Src/WS2812B/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F103xB -I"C:/Users/sloth/Desktop/WS2812_LED/Inc" -I"C:/Users/sloth/Desktop/WS2812_LED/Drivers/STM32F1xx_HAL_Driver/Inc" -I"C:/Users/sloth/Desktop/WS2812_LED/Drivers/STM32F1xx_HAL_Driver/Inc/Legacy" -I"C:/Users/sloth/Desktop/WS2812_LED/Drivers/CMSIS/Device/ST/STM32F1xx/Include" -I"C:/Users/sloth/Desktop/WS2812_LED/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


